package model.data_structures;

import java.util.EmptyStackException;
import java.util.Iterator;

public class Stack<T> implements IPila<T> 
{
	private ListaEncadenada l;

	public Stack() 
	{
		// TODO Auto-generated constructor stub
		ListaEncadenada l = new ListaEncadenada<>();
	}
	@Override
	public void push (T item)  {

		l.agregarElementoFinal(item);
	}

	public T pop () 
	{
		Nodo<T> nNodo = new Nodo((T) l.darCabeza());
		if(l.darCabeza() == null)
		{
			throw  new EmptyStackException();
		}
		l.eliminarElemento(l.darNumeroElementos());

		return (T) nNodo;
	}

	public boolean isEmpty() 
	{
		if(l.darCabeza() == null && l.darNumeroElementos() == 0)
		{
			return true;
		}
		return false;
	}

	public int size ()
	{
		return l.darNumeroElementos();
	}
}

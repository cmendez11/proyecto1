package model.data_structures;

public interface IPila<T>
{
	public void push(T item);

	public boolean isEmpty();

	public T pop ();

	public int size ();
}

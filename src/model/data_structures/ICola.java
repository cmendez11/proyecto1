package model.data_structures;

public interface ICola<T>
{

	public void enqueue(T item);

	public boolean isEmpty();

	public T dequeue ();

	public int size ();

}

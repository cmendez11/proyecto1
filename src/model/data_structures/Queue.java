package model.data_structures;

import java.util.EmptyStackException;
import java.util.Iterator;

public class Queue<T> implements ICola<T>
{

	private ListaEncadenada l;

	public Queue()
	{
		l = new ListaEncadenada<>(); 
	}
	@Override
	public void enqueue(T item) 
	{
		l.agregarElementoFinal(item);
	}

	@Override
	public boolean isEmpty() 
	{
		if(l.darCabeza() == null && l.darNumeroElementos() == 0)
		{
			return true;
		}
		return false;
	}

	@Override
	public T dequeue() 
	{
		if(l.darCabeza() == null)
		{
			throw  new EmptyStackException();
		}
		l.eliminarElemento(0);

		Nodo<T> nNodo = new Nodo((T) l.darCabeza());
		return (T) nNodo;
	}

	@Override
	public int size() 
	{
		return l.darNumeroElementos();
	}

}
